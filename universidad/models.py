from django.db import models

#  Universidad de los LLanos,
# Especialización Ingenieria de Software
# Jorge Serrano


USER_TYPES = [
    ('PROFESOR', 'profesor'),
    ('ESTUDIANTE', 'estudiante'),
    ('DECANO', 'decano')
]


class EduUser(models.Model):
    edu_id = models.CharField(
        max_length=64
    )
    user_type = models.CharField(
        max_length=16,
        choices=USER_TYPES
    )
    nombres = models.CharField(
        max_length=255
    )
    apellidos = models.CharField(
        max_length=255
    )
    correo = models.CharField(
        max_length=255,
        blank=True
    )
    celular = models.CharField(
        max_length=32,
        blank=True
    )
    direccion = models.CharField(
        max_length=64,
        blank=True
    )


class Asignatura(models.Model):
    codigo_id = models.CharField(
        max_length=64
    )
    nombre = models.CharField(
        max_length=255
    )

    profesor_principal = models.ForeignKey(
        EduUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    profersores = models.ManyToManyField(
        EduUser,
        related_name='profesor_asignatura'
    )
    estudiantes = models.ManyToManyField(
        EduUser,
        related_name='estudiante_asignatura'
    )


class Facultad(models.Model):
    codigo_id = models.CharField(
        max_length=64
    )
    nombre = models.CharField(
        max_length=255
    )
    decano = models.ForeignKey(
        EduUser,
        on_delete=models.SET_NULL,
        blank=True,
        null=True
    )
    asignaturas = models.ManyToManyField(
        Asignatura,
        related_name='facultad_asignatura'
    )
