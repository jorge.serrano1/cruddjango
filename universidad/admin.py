from django.contrib import admin

# Register your models here.
# Register your models here.
from universidad.models import EduUser,Facultad,Asignatura;

class UserAdmin(admin.ModelAdmin):
    list_display = ('edu_id', 'user_type', 'nombres', 'apellidos',
                    'correo', 'celular', 'direccion')


class AsignaturasAdmin(admin.ModelAdmin):
    list_display = ('codigo_id', 'nombre', 'profesor_principal')


class FacultadesAdmin(admin.ModelAdmin):
    list_display = ('codigo_id', 'nombre')


admin.site.register(EduUser, UserAdmin)
admin.site.register(Asignatura, AsignaturasAdmin)
admin.site.register(Facultad, FacultadesAdmin)